FROM alpine:edge

ARG BUILD_ID
ENV VERSION $BUILD_ID
COPY dist/ /slackbot
COPY LICENCE /slackbot
RUN chmod a+x /slackbot/test.sh && \
    mkdir /var/lib/slackbot/ && \
    apk add \
        --no-cache \
        -X http://dl-cdn.alpinelinux.org/alpine/edge/testing \
        pike-pcre \
        dumb-init

VOLUME /var/lib/slackbot/
WORKDIR /slackbot/
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["pike", "-Mpike-modules", "start.pike", "--runtime", "/data"]

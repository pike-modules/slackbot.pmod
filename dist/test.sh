#!/bin/sh

cd /slackbot || exit 1

pike -Mtests/mock-modules -Mpike-modules pike-modules/Testing.pmod/runner.pike tests/*

# vim:set et sw=4 ts=4 fdm=indent:

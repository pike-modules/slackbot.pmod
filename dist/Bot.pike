inherit Slack.Bot;


#include "version.h"


private Config.Config config;
private array(object) message_handlers = ({ });
private object admin_module;

mixed query(string what)
{
    mixed ret;

    mixed err = catch {
        ret = config["global"][what];
    };

    return err ? UNDEFINED : ret;
}

mapping(string:array(string)) get_commands(string channel)
{
    mapping(string:array(string)) ret = ([ ]);
    foreach (message_handlers; int index; object ob)
    {
        bool is_handler_whitelisted =
            has_value(indices(query("module whitelist")), ob->name);
        bool is_channel_whitelisted = is_handler_whitelisted
            && has_value(query("module whitelist")[ob->name], channel);

        if ((is_handler_whitelisted && is_channel_whitelisted)
            || !is_handler_whitelisted)
        {
            ret += ob->get_commands();
        }
    }

    return ret;
}

Config.Config.Section get_section(string name)
{
    return config[name] || Config.Config.Section(name);
}

void create(string tok, void|bool no_modules, void|string runtime)
{
    string cfg_file = combine_path(runtime || "./", "config.json");
    config = Config.Config(cfg_file);

    define_variables();

    config["global"]["runtime"] = runtime;

    add_constant("BOT", this);

    array(object) handlers = preload_modules();

    if (Stdio.exist(cfg_file))
        config->read();
    else
    {
        config->write();
        return;
    }

    ::create(query("botname"), tok);

    load_modules(handlers);
    config->write();
}

void write_cfg()
{
    config->write();
}

void defvar(string section, string name, string description,
    Config.Variable type, void|mixed def)
{
    config->add_variable(section, name, description, type, def);
}

private void define_variables()
{
    void defgvar(string name, string description,
        Config.Variable type, void|mixed def)
    {
        defvar("global", name, description, type, def);
    };

    defgvar("admins", "List of user names that can run !admin command",
            Config.Array, ({ }));
    defgvar("disabled", "List of dissabled modules",
            Config.Array, ({ }));
    defgvar("module whitelist", "If module is present int this "
            "list, it should be allowed only on those channels. "
            "Format: <chan name>: ({ channels, })", Config.Mapping, ([ ]));
    defgvar("message handlers path", "Path to message handlers",
            Config.String, "message_handlers");
    defgvar("runtime", "Path for all the runtime fils",
            Config.String, "./");
}

private object load_single_module(string path)
{
    program handler;
    object handlero;
    mixed e = catch {
        handler = compile_string(Stdio.read_file(path), path);
        handlero = handler();
    };

    if (e)
    {
        werror("Unable to load module: %s\n%'-'80s\n%O\n%'-'80s\n", path, " ",
            describe_backtrace(e), " ");
        return UNDEFINED;
    }

    return handlero;
}

private array(object) preload_modules()
{
    if (!message_handlers)
        message_handlers = ({ });

    string handlers_dir = query("message handlers path");
    array(string) files = get_dir(handlers_dir) || ({ });

    array(object) ret = ({ });

    foreach (files, string path)
    {
        if (path[<3..] != "pike")
            continue;

        object module = load_single_module(combine_path(handlers_dir, path));

        if (module)
            ret += ({ module });
    }

    admin_module = load_single_module("admin.pike");

    return ret;
}

private void load_modules(array(object) preloaded)
{
    bool cmp_handlers(object ob1, object ob2)
    {
        return ob1->priority > ob2->priority;
    };

    foreach (preloaded, object handlero)
    {
        if (has_value(query("disabled"), handlero->name))
        {
            write("Not loading message handler: %s (disabled in config)\n",
                handlero->name);
            destruct(handlero);
            continue;
        }

        if (handlero->name && handlero->applies && handlero->get_action)
        {
            handlero->init();
            message_handlers += ({ handlero });
            message_handlers = Array.sort_array(message_handlers, cmp_handlers);
            write("Loading message handler: %s\n", handlero->name);
        }
        else
        {
            werror("Couldn't load message handler: %s\n", handlero->name);
            destruct(handlero);
        }
    }
}

private void respond(array actions, object module)
{
    foreach (actions; int index; mapping single_action)
    {
        mapping resp = UNDEFINED;
        if (single_action->type == "api")
        {
            resp = Slack.call(single_action->api, token,
                    single_action->params, single_action->headers,
                    single_action->data);
        }
        else
            send(single_action->type, single_action->params);

        if (resp && !resp->ok)
        {
            werror("[%s] Error while sending %s: %O\n",
                module->name, single_action->api, resp);
        }
    }
}

string version()
{
    return sprintf("%d.%d.%d",
                    BOT_MAJOR_VERSION,
                    BOT_MINOR_VERSION,
                    (int)getenv("CI_BUILD_ID"));
}

void message(mapping(string:mixed) data)
{
    if (admin_module->applies(data))
    {
        array(object) modules_with_admin =
            filter(message_handlers, lambda(object x) {
                return functionp(x->admin);
                });

        array resp = admin_module->get_response(data, modules_with_admin);
        respond(resp, admin_module);
    }

    foreach (message_handlers, object handler)
    {
        string name = handler->name;
        if (query("module whitelist")[?name] && data->channel)
        {
            string chname = (get_channel(data->channel)
                            || get_group(data->channel))[?"name"];

            if (!chname)
                chname = get_user(data->user)[?"name"];

            if (!has_value(query("module whitelist")[name], chname))
                continue;
        }

        if (handler->applies && handler->applies(data))
        {
            mapping|array actions = handler->get_action(data);

            if (!actions)
                continue;

            if (mappingp(actions))
                actions = ({ actions });

            respond(actions, handler);

            data->was_handled = true;
        }
    }
}

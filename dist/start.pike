import ".";

string read_token(string path)
{
    return String.trim_all_whites(Stdio.read_file(path));
}

int main(int c, array(string) args)
{
    bool save = false;
    string runtime = "./";

    array(array(string|int|array(string))) opts = ({
        ({ "save",     Getopt.NO_ARG, "--save" }),
        ({ "runtime",  Getopt.HAS_ARG, "--runtime" }),
    });

    foreach(Getopt.find_all_options(args, opts), array opt)
    {
        switch(opt[0])
        {
            case "save":
                save = true;
                break;

            case "runtime":
                runtime = (string)opt[1];
                break;
        }
    }

    string config_path = combine_path(runtime, "config.json");
    string token_path = combine_path(runtime, ".bottoken");

    if (!Stdio.exist(config_path) || save)
    {
        werror("Saving config. Edit and start again\n");
        Bot("", true, runtime)->write_cfg();
        return 0;
    }

    werror("SlackAPI test: %O\n", Slack.api_test());

    string token = read_token(token_path);
    Bot bot = Bot(token, 0, runtime);
    return bot->connect();
}

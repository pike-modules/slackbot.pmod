import ".";

string token;

#define F(X) void|mixed X(void|mixed  ... args) { return Slack.t_next_response(); }
F(connect);
F(get_name);
F(get_id);
F(get_prefs);
F(get_pref);
F(get_self);
F(get_user);
F(get_channel);
F(get_group);
F(send);
F(create);
F(ping);
F(ping_timeout);
F(got_data);
F(channel_created);
F(channel_joined);
F(group_created);
F(group_joined);
F(hello);
F(pong);
F(presence_change);
F(reconnect_url);
F(team_join);
F(user_change);
F(user_typing);


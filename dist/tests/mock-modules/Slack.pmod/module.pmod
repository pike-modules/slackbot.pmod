array(mixed) responses = ({ });

mixed t_next_response() {
    mixed ret;

    if (sizeof(responses))
    {
        ret = responses[0];
        responses = responses[1..];
    }

    return ret;
}

#define F(X) void|mixed X(void|mixed ... args) { return t_next_response(); }
F(call);
F(parse_response);
F(api_test);
F(rtm_start);

import ".";
inherit Testing.TestCase;

#include "macros.h"
#include "utils.h"

private Testing.Mock _message_handler;

private mapping(string:string|int)
mkdata(string ch, string ts, string text, void|mapping(string:string|int) misc)
{
    mapping t = Calendar.parse("%h:%m:%s", ts)
        || Calendar.parse("%a.%M.%Y %h:%m:%s", ts)
        || ([ "datetime": lambda() { return ([ "unix": 0 ]); } ]);

    return ([
        "channel": ch,
        "text": text,
        "ts": t->datetime()->unix,
    ]) + (misc || ([ ]));
}

void tear_up()
{
    array vars = ({
        ({ "global", "debug", "debug", Config.Bool, false }),
    });

    TestBot.TestBot _bot = TestBot.TestBot("tests/1337.json", vars);
    add_constant("BOT", _bot);

    _bot->add_user("U1", "User1");
    _bot->add_user("U2", "User2");
    _bot->add_user("U3", "User3");
    string handler_file = "message_handlers/1337.pike";
    _message_handler = Testing.Mock(handler_file);

    _bot->config->read();
}

void tear_down()
{
    _message_handler = 0;
    add_constant("BOT");
}

bool test_apply()
{
    ASSERT_TRUE(_message_handler()->applies(([
        "text": "13:37"
    ])));
    ASSERT_TRUE(_message_handler()->applies(([
        "text": "!leetscore"
    ])));
    ASSERT_FALSE(_message_handler()->applies(([
        "text": "13:36"
    ])));
    ASSERT_FALSE(_message_handler()->applies(([
        "text": "13:37",
        "bot_id": "testid"
    ])));
    ASSERT_FALSE(_message_handler()->applies(UNDEFINED));
    ASSERT_FALSE(_message_handler()->applies(([ ])));

    return true;
}

bool test_get_commands()
{
    array(string) commands = ({ "!leetscore", "13:37" });
    ASSERT_EQUAL(_message_handler()->get_commands(), ([ "1337" : commands ]));

    return true;
}

bool test_year()
{
    _message_handler->set_header("#define private ");
    object c = _message_handler().Channel("test_channel");
    object y = _message_handler().Year(2016, c);

    ASSERT_EQUAL(y->year, 2016);
    ASSERT_EQUAL(sprintf("%s", y), "1337_year(2016, #weeks: 0)");
    ASSERT_TRUE(y->is_empty());
    ASSERT_EQUAL(y->weeks, ([ ]));
    ASSERT_EQUAL(y->to_int(), 2016);
    ASSERT_EQUAL(y->to_string(), "2016");
    ASSERT_TRUE(y->add(1)->is_empty());
    ASSERT_EQUAL(sprintf("%s", y), "1337_year(2016, #weeks: 1)");
    ASSERT_TRUE(sizeof(y->weeks) == 1);
    ASSERT_FALSE(y->is_empty());
    ASSERT_TRUE(y[1]->is_empty());
    ASSERT_TRUE(y[2]->is_empty());
    ASSERT_TRUE(y[2]->attempt(0, "U1"));
    ASSERT_TRUE(y[2]->is_empty());
    ASSERT_TRUE(y[1]->attempt(0, "U1"));
    ASSERT_FALSE(y[1]->is_empty());

    return true;
}

bool test_week()
{
    string target = "*Week 1*, 2016:\n\n"
        "``` 1. User1      2\n"
        " 2. User2      1\n\n"
        "Monday          User1\n"
        "Tuesday         User1\n"
        "Wednesday       User2\n"
        "Thursday             \n"
        "Friday               \n"
        "Saturday             \n"
        "Sunday               \n```";

    _message_handler->set_header("#define private ");
    object c = _message_handler().Channel("test_channel");
    object y = _message_handler().Year(2016, c);
    object w = _message_handler().Week(1, y);

    ASSERT_EQUAL(w->year, y);
    ASSERT_EQUAL(w->week, 1);
    ASSERT_TRUE(w->is_empty());
    ASSERT_EQUAL(w->days, allocate(7));
    ASSERT_EQUAL(w[0], 0);
    ASSERT_EQUAL(w->to_string(), "1");
    ASSERT_EQUAL(w->to_int(), 1);
    ASSERT_EQUAL(sprintf("%s", w), "1337_week(1, y: 2016)");
    ASSERT_TRUE(w->attempt(0, "U1"));
    ASSERT_FALSE(w->attempt(0, "U1"));
    ASSERT_FALSE(w->attempt(0, "U2"));
    ASSERT_FALSE(w->is_empty());
    ASSERT_EQUAL(w->days, ({ "U1" }) + allocate(6) );
    ASSERT_TRUE(w->attempt(1, "U1"));
    ASSERT_TRUE(w->attempt(2, "U2"));
    ASSERT_EQUAL(w->days, ({ "U1", "U1", "U2", 0, 0, 0, 0 }));
    ASSERT_EQUAL(w->describe(), target);
    w->set(1, "U10");
    w->set(4, "U11");
    ASSERT_EQUAL(w->days, ({ "U1", "U10", "U2", 0, "U11", 0, 0 }));

    return true;
}

bool test_make_tree()
{
    _message_handler->set_header("#define private ");
    string ch1 = "ch1";
    ASSERT_EQUAL(_message_handler()->channels, ([ ]));
    ASSERT_EQUAL(_message_handler()->make_tree(ch1, 2017, 53, 7), ({ 2016, 52 }));
    ASSERT_EQUAL(_message_handler()->make_tree(ch1, 2017, 1, 0), ({ 2017, 1 }));
    ASSERT_EQUAL(sizeof(_message_handler()->channels), 1);
    ASSERT_EQUAL(sizeof(_message_handler()->channels[ch1]->years), 2);
    ASSERT_TRUE(_message_handler()->channels[ch1][2016]);
    ASSERT_EQUAL(sizeof(_message_handler()->channels[ch1][2016]->weeks), 1);
    return true;
}

bool test_admin_set_today()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);
    mapping(string:string|int) data = ([
            "text": "!admin 1337 set today testuser",
            "channel": "ch1",
    ]);

    mapping(string:string|int) today = Calendar.Day("unix", time())->datetime();
    array(string) expected_today = allocate(7);
    expected_today[(!today->week_day ? 7 : today->week_day)-1] = "U1";

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "No such user: \"testuser\"");
    data->text = "!admin 1337 set today User1";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    ASSERT_EQUAL(
            _message_handler()->channels["ch1"][today->year][today->week]->days,
            expected_today);
    return true;
}

bool test_admin_set_yesterday()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);
    mapping(string:string|int) data = ([
            "text": "!admin 1337 set yesterday testuser",
            "channel": "ch1",
    ]);

    mapping(string:string|int) yday =
        Calendar.Day("unix", time()-(24*60*60))->datetime();
    array(string) expected_yesterday = allocate(7);
    expected_yesterday[(!yday->week_day ? 7 : yday->week_day)-1] = "U1";

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "No such user: \"testuser\"");
    data->text = "!admin 1337 set yesterday User1";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    ASSERT_EQUAL(
            _message_handler()->channels["ch1"][yday->year][yday->week]->days,
            expected_yesterday);
    return true;
}

bool test_admin_set_minus_day()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);
    mapping(string:string|int) data = ([
            "text": "!admin 1337 set -2 testuser",
            "channel": "ch1",
    ]);

    mapping(string:string|int) day =
        Calendar.Day("unix", time()-(2*24*60*60))->datetime();
    array(string) expected_day = allocate(7);
    expected_day[(!day->week_day ? 7 : day->week_day)-1] = "U1";

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "No such user: \"testuser\"");
    data->text = "!admin 1337 set -2 User1";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    ASSERT_EQUAL(
            _message_handler()->channels["ch1"][day->year][day->week]->days,
            expected_day);
    return true;
}

bool test_admin_set_date()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);
    mapping(string:string|int) data = ([
            "text": "!admin 1337 set 31.12.2017 testuser",
            "channel": "ch1",
    ]);

    mapping(string:string|int) day =
        Calendar.parse("%a.%M.%Y", "11.01.1986")->datetime();
    array(string) expected_day = allocate(7);
    expected_day[(!day->week_day ? 7 : day->week_day)-1] = "U1";

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "No such user: \"testuser\"");
    data->text = "!admin 1337 set 11-01-1986 User1";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Wrong time format.");
    data->text = "!admin 1337 set 11.01.1986 User1";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    ASSERT_EQUAL(
            _message_handler()->channels["ch1"][day->year][day->week]->days,
            expected_day);
    return true;
}

bool test_admin_config_num_of_days()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);

    mapping(string:string|int) data = ([
            "text": "!admin 1337 config num_of_week_days 1",
            "channel": "ch1",
    ]);

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    data->text = "!admin 1337 config num_of_week_days 0";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Something went wrong.");
    data->text = "!admin 1337 config num_of_week_days 8";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Something went wrong.");
    data->text = "!admin 1337 config num_of_week_days 7";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Done");
    data->text = "!admin 1337 config num_of_week_days";
    resp = _message_handler()->admin(data);
    ASSERT_EQUAL(get_resp_text(resp), "Missing some arguments to `config`.");
    return true;
}

bool test_admin_config_offset()
{
    _message_handler->set_header("#define private ");
    string fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);

    mapping(string:string|int) data = ([
            "text": "!admin 1337 config offset 1",
            "channel": "ch1",
    ]);

    array(mapping(string:string|int)) resp = _message_handler()->admin(data);
    ASSERT_EQUAL(_message_handler()->get_channel(data->channel)->offset, 1);
    return true;
}

bool test_encode_week()
{
    mapping target = ([
            "week": 1,
            "days": ({ "U1", 0, "U2", 0, 0, 0, 0 }),
    ]);
    object c = _message_handler().Channel("test_channel");
    object w = _message_handler().Week(1, _message_handler().Year(2016, c));

    ASSERT_TRUE(w->attempt(0, "U1"));
    ASSERT_TRUE(w->attempt(2, "U2"));
    ASSERT_EQUAL(Standards.JSON.decode(w->encode_json()), target);
}

bool test_encode_year()
{
    mapping target = ([
            "year": 2016,
            "weeks": ([
                "1": ([
                    "week": 1,
                    "days": ({ "U1", 0, 0, 0, 0, 0, 0 }),
                ])
            ])
    ]);
    object c = _message_handler().Channel("test_channel");
    object y = _message_handler().Year(2016, c);

    y->add(1);
    ASSERT_TRUE(y[1]->attempt(0, "U1"));
    ASSERT_EQUAL(Standards.JSON.decode(y->encode_json()), target);
}

bool test_encode_channel()
{
    mapping target = ([
            "years": ([
                "2016": ([
                    "year": 2016,
                    "weeks": ([
                        "1": ([
                            "week": 1,
                            "days": ({ "U1", 0, 0, 0, 0, 0, 0 }),
                        ])
                    ])
                ])
            ]),
            "num_of_week_days": 7,
            "offset": 1
    ]);

    object c = _message_handler().Channel("test");

    c->add(2016)->add(1);
    ASSERT_TRUE(c[2016][1]->attempt(0, "U1"));
    ASSERT_EQUAL(Standards.JSON.decode(c->encode_json()), target);
}

bool test_load_week()
{
    _message_handler->set_header("#define private ");
    mapping source = ([
            "week": 1,
            "days": ({ "U1", 0, "U2", 0, 0, 0, 0 }),
    ]);
    object c = _message_handler().Channel("test_channel");
    object w = _message_handler().Week(1, _message_handler().Year(2016, c));

    w->load(source);
    ASSERT_EQUAL(w->week, 1);
    ASSERT_EQUAL(w->days, source->days);
}

bool test_load_year()
{
    _message_handler->set_header("#define private ");
    mapping source = ([
            "year": 2016,
            "weeks": ([
                "1": ([
                    "week": 1,
                    "days": ({ "U1", 0, 0, 0, 0, 0, 0 }),
                ])
            ])
    ]);
    object c = _message_handler().Channel("test_channel");
    object y = _message_handler().Year(2016, c);
    y->load(source);

    ASSERT_EQUAL(sizeof(y->weeks), 1);
    ASSERT_EQUAL(y->year, 2016);
    ASSERT_EQUAL(y[1]->days, source->weeks["1"]->days);
}

bool test_load_channel()
{
    _message_handler->set_header("#define private ");
    mapping source = ([
            "years":([
                "2016": ([
                    "year": 2016,
                    "weeks": ([
                        "1": ([
                            "week": 1,
                            "days": ({ "U1", 0, 0, 0, 0, 0, 0 }),
                        ])
                    ])
                ])
            ])
    ]);

    object c = _message_handler().Channel("test");
    c->load(source);

    ASSERT_EQUAL(sizeof(c->years), 1);
    ASSERT_EQUAL(c[2016]->year, 2016);
    ASSERT_EQUAL(c[2016][1]->days, source["years"]["2016"]["weeks"]["1"]->days);
}

bool test_check_if_hit()
{
    constant success_prefix = "13:37 indeed";
    constant almost_prefix = "So close";
    constant failed_prefix = "Nope, not even close";
    constant congrats = "and you were fastest gun today";
    constant fapped = "You fapped";

    string fmock = "mapping check_if_hit(mapping(string:mixed) data) "
                    "{ return mocked_check_if_hit(data); }";
    _message_handler->mock("check_if_hit", "mapping", "private", fmock);
    fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);

    mapping(string:mixed) base = ([
        "user": "U123",
    ]);

    mapping(string:mixed) data = mkdata("test ch", "13:36:00", "!admin 1337 config offset 0", base);
    _message_handler()->admin(data);
    data = mkdata("test ch", "13:37:00", "13:37", base);
    mapping resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], success_prefix));
    ASSERT_TRUE(has_value(resp["params"]["text"], congrats));

    data = mkdata("test ch", "13:37:59", "13:37", base);
    resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], success_prefix));
    ASSERT_FALSE(has_value(resp["params"]["text"], congrats));

    data = mkdata("test ch", "13:38:01", "13:37", base);
    resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], almost_prefix));
    ASSERT_FALSE(has_value(resp["params"]["text"], congrats));

    data = mkdata("test ch", "13:36:59", "13:37", base);
    resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], fapped));

    data = mkdata("test ch", "13:39:01", "13:37", base);
    resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], failed_prefix));
    ASSERT_FALSE(has_value(resp["params"]["text"], congrats));

    data = mkdata("test ch", "13:35:59", "13:37", base);
    resp = _message_handler()->check_if_hit(data);
    ASSERT_TRUE(has_prefix(resp["params"]["text"], failed_prefix));
    ASSERT_FALSE(has_value(resp["params"]["text"], congrats));

    return true;
}

bool test_get_score_current()
{
    string target = "*Week 1*, 2016:\n\n"
        "``` 1. User1      4\n"
        " 2. User2      1\n\n"
        "Monday          User1\n"
        "Tuesday         User2\n"
        "Wednesday       User1\n"
        "Thursday        User1\n"
        "Friday          User1\n"
        "Saturday             \n"
        "Sunday               \n```";

    string fmock = "mapping check_if_hit(mapping(string:mixed) data) "
                    "{ return mocked_check_if_hit(data); }";
    _message_handler->mock("check_if_hit", "mapping", "private", fmock);
    fmock = "mapping get_score(mapping(string:mixed) data) "
                    "{ return mocked_get_score(data); }";
    _message_handler->mock("get_score", "mapping", "private", fmock);
    fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);

    mapping(string:mixed) user1 = ([
        "user": "U1",
    ]);

    mapping(string:mixed) user2 = ([
        "user": "U2",
    ]);

    mapping(string:mixed) data =
        mkdata("test ch", "13:36:00", "!admin 1337 config offset 0", user1);
    _message_handler()->admin(data);
    data = mkdata("test ch", "01.01.2016 13:37:00", "!leetscore", user1);
    ASSERT_TRUE(_message_handler()->get_score(data) != "");
    data =
        mkdata("test ch", "01.01.2016 13:37:00", "!leetscore week 1 2016", user1);
    ASSERT_TRUE(_message_handler()->get_score(data) != "");
    data = mkdata("test ch", "04.01.2016 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "05.01.2016 13:37:00", "13:37", user2);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "06.01.2016 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "07.01.2016 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "08.01.2016 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "08.01.2016 13:37:10", "!leetscore", user1);
    ASSERT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);
    data =
        mkdata("test ch", "08.01.2016 13:37:15", "!leetscore week 1 2016", user1);
    ASSERT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);
    data = mkdata("test ch", "08.01.2016 13:37:15", "!leetscore 1 2016", user1);
    ASSERT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);
    data = mkdata("test ch", "08.01.2016 13:37:15", "!leetscore week", user1);
    ASSERT_NOT_EQUAL(_message_handler()->get_score(data)["params"]["text"],
            target);

    return true;
}

bool test_new_year_change()
{
    string target = "*Week 53*, 2015:\n\n"
        "``` 1. User1      4\n"
        " 2. User2      3\n\n"
        "Monday          User1\n"
        "Tuesday         User2\n"
        "Wednesday       User1\n"
        "Thursday        User1\n"
        "Friday          User1\n"
        "Saturday        User2\n"
        "Sunday          User2\n```";

    string fmock = "mapping check_if_hit(mapping(string:mixed) data) "
                    "{ return mocked_check_if_hit(data); }";
    _message_handler->mock("check_if_hit", "mapping", "private", fmock);
    fmock = "mapping get_score(mapping(string:mixed) data) "
                    "{ return mocked_get_score(data); }";
    _message_handler->mock("get_score", "mapping", "private", fmock);
    fmock = "private void save() { ; }";
    _message_handler->mock("save", "void", "private", fmock);

    mapping(string:mixed) user1 = ([
        "user": "U1",
    ]);

    mapping(string:mixed) user2 = ([
        "user": "U2",
    ]);

    mapping(string:mixed) data =
        mkdata("test ch", "13:36:00", "!admin 1337 config offset 0", user1);
    _message_handler()->admin(data);
    data = mkdata("test ch", "28.12.2015 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "29.12.2015 13:37:00", "13:37", user2);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "30.12.2015 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "31.12.2015 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "01.01.2016 13:37:00", "13:37", user1);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "02.01.2016 13:37:00", "13:37", user2);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "03.01.2016 13:37:00", "13:37", user2);
    _message_handler()->check_if_hit(data);
    data = mkdata("test ch", "03.01.2016 13:37:10", "!leetscore", user1);
    ASSERT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);
    data = mkdata("test ch", "08.01.2016 13:37:15", "!leetscore week 53 2015", user1);
    ASSERT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);
    data = mkdata("test ch", "08.01.2016 13:37:15", "!leetscore week", user1);
    ASSERT_NOT_EQUAL(_message_handler()->get_score(data)["params"]["text"], target);

    return true;
}

bool test_score_table_add()
{
    object score_table = _message_handler().ScoreTable();
    score_table->add("test");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 1 }));
    score_table->add("test");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 2 }));
    score_table->add("test2");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test", "test2" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 2, 1 }));

    return true;
}

bool test_score_table_add_with_swap()
{
    object score_table = _message_handler().ScoreTable();
    score_table->add("test");
    score_table->add("test");
    score_table->add("test2");
    score_table->add("test2");
    score_table->add("test2");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test2", "test" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 3, 2 }));

    return true;
}

bool test_score_table_add_with_multi_swap()
{
    object score_table = _message_handler().ScoreTable();
    score_table->add("test");
    score_table->add("test");
    score_table->add("test2");
    score_table->add("test2");
    score_table->add("test3");
    score_table->add("test3");
    score_table->add("test3");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test3", "test", "test2" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 3, 2, 2 }));

    return true;
}

bool test_score_table_add_with_no_swap()
{
    object score_table = _message_handler().ScoreTable();
    score_table->add("test");
    score_table->add("test");
    score_table->add("test2");
    score_table->add("test2");
    ASSERT_EQUAL(score_table->get_scores()->name, ({ "test", "test2" }));
    ASSERT_EQUAL(score_table->get_scores()->score, ({ 2, 2 }));

    return true;
}

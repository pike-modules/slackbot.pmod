private string get_resp_text(array(mapping(string:string|int)) data)
{
    return data[0]->params->text;
}

// vim:set et sw=4 ts=4 ft=pike:

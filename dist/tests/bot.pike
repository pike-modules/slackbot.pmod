import "..";
import ".";
inherit Testing.TestCase;

#include "macros.h"

private Testing.Mock _bot;

void tear_up()
{
    array vars = ({
        ({ "global", "debug", "debug", Config.Bool, false }),
        ({ "global", "admins", "admins", Config.Array, ({ }) }),
    });

    string bot_file = "Bot.pike";
    _bot = Testing.Mock(bot_file, true);
    _bot->set_header("#define private ");
    _bot->mock("create", "void", "", "void create() { }");
    _bot->set_lazy_recompile(false);
    _bot->recompile();
}

void tear_down()
{
    _bot = 0;
}

bool test_respond()
{
    class MessageHandler(string name) { };
    array(mapping(string:string|int)) actions = ({
            ([
                 "api": "A",
                 "headers": "",
                 "params": "A",
                 "data": "a"
            ]),
        });
    _bot()->respond(actions, MessageHandler("mh1"));
    return true;
}

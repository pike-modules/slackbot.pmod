#define ASSERT_TRUE(args...) assert_true(__FILE__, __LINE__, args)
#define ASSERT_FALSE(args...) assert_false(__FILE__, __LINE__, args)
#define ASSERT_EQUAL(args...) assert_equal(__FILE__, __LINE__, args)
#define ASSERT_NOT_EQUAL(args...) assert_not_equal(__FILE__, __LINE__, args)

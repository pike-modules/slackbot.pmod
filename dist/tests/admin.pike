import "..";
import ".";
inherit Testing.TestCase;

#include "macros.h"

private Testing.Mock _admin;

void tear_up()
{
    array vars = ({
        ({ "global", "debug", "debug", Config.Bool, false }),
        ({ "global", "admins", "admins", Config.Array, ({ }) }),
    });

    TestBot.TestBot bot = TestBot.TestBot("tests/admin.json", vars);

    add_constant("BOT", bot);

    string admin_file = "admin.pike";
    _admin = Testing.Mock(admin_file);
    _admin->set_header("import \"pike-modules\";");

    bot->config->read();
}

void tear_down()
{
    _admin = 0;
    add_constant("BOT");
}

bool test_apply()
{
    mapping(string:string|int) data = ([
        "user": "aaa",
        "text": "!admin"
    ]);
    ASSERT_TRUE(_admin()->applies(data));
    ASSERT_EQUAL(data->admin, 1);
    ASSERT_EQUAL(data->admin_granted, 1);
    data = ([
        "user": "bbb",
        "text": "!admin"
    ]);
    ASSERT_TRUE(_admin()->applies(data));
    ASSERT_EQUAL(data->admin, -1);
    ASSERT_EQUAL(data->admin_granted, UNDEFINED);
    ASSERT_FALSE(_admin()->applies(UNDEFINED));
    ASSERT_FALSE(_admin()->applies(([ ])));

    return true;
}

bool test_get_response()
{
    mapping(string:string|int) mkdata(string text)
    {
        return ([
                "user": "U1234",
                "channel": "C112344",
                "text": text,
                "ts": time(),
        ]);
    };

    class Module(string name) { ; };
    class Adminable {
        inherit Module;
        array(mapping) admin(mapping data)
        {
            return ({ ([ "response": name ]) });
        }
    };

    object m1 = Adminable("module1");
    object m2 = Adminable("module2");

    mapping(string:string|int) data = mkdata("!admin");
    ASSERT_TRUE(_admin()->applies(data));
    ASSERT_EQUAL(data->admin, 1);
    ASSERT_EQUAL(data->admin_granted, 1);
    array(mapping(string:string|int)) expected = ({
            Utils.simple_response(data,
                    "Admin rights granted.")
            });
    ASSERT_EQUAL(_admin()->get_response(data, ({ m1, m2 })), expected);
    data = mkdata("!admin");
    data->user = "U123";
    ASSERT_TRUE(_admin()->applies(data));
    ASSERT_EQUAL(data->admin, -1);
    ASSERT_EQUAL(data->admin_granted, UNDEFINED);
    ASSERT_TRUE(has_prefix(
                _admin()->get_response(data, ({ m1, m2 }))[0]->params["text"],
                "There can be only one! "));
    ASSERT_TRUE(_admin()->get_response(mkdata("!admin module2"), ({ m1, m2 })));
    expected = ({
            Utils.simple_response(mkdata("!admin module"),
                    "There is no such module or command: \"module\"")
            });
    ASSERT_EQUAL(_admin()->get_response(mkdata("!admin module"), ({ m1, m2 })),
            expected);
    ASSERT_EQUAL(_admin()->get_response(mkdata("!admin module2"), ({ m1, m2 })),
            ({ ([ "response": "module2" ]) }));
    expected = ({
            Utils.simple_response(mkdata("!admin modules"),
                    "Available modules:\n - module1\n - module2\n")
            });
    ASSERT_EQUAL(_admin()->get_response(mkdata("!admin modules"), ({ m1, m2 })),
            expected);
    expected = ({
            Utils.simple_response(mkdata("!admin help"),
                    "Available commands:\n - modules\n - ls\n - admins\n"
                    " - moderators\n - help")
            });
    ASSERT_EQUAL(_admin()->get_response(mkdata("!admin help"), ({ m1, m2 })),
            expected);

    return true;
}

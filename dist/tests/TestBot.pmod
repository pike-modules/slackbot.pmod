import "../pike-modules";
import "..";
import Slack.Objects;

class TestBot
{
    Config.Config config;
    mapping(string:User) users = ([ ]);
    mapping(string:Channel) channels = ([ ]);

    void create(string cfg_file, array vars)
    {
        config = Config.Config(cfg_file, false);

        foreach (vars, array var)
            config->add_variable(@var);
    }

    Config.Config.Section get_section(string name)
    {
        return config[?name];
    }

    void defvar(string section, string name, string description,
        Config.Variable type, void|mixed def)
    {
        config->add_variable(section, name, description, type, def);
    }

    mixed query(string what)
    {
        mixed ret;

        mixed err = catch {
            ret = config["global"][what];
        };

        return err ? UNDEFINED : ret;
    }

    void write_cfg() { };

    User get_user(string uid)
    {
        return users[uid];
    }

    User get_user_by_name(string name)
    {
        foreach (users; string uid; User user)
        {
            if (user->name == name)
                return user;
        }
    }

    void set_user(string uid, mapping(string:string|int) data)
    {
        users[uid] = User(data);
    }

    User add_user(string uid, string name)
    {
        users[uid] = User(([
            "id": uid,
            "name": name,
        ]));

        return users[uid];
    }

    Channel get_channel(string cid)
    {
        return channels[cid];
    }

    void set_channel(string cid, mapping(string:string|int) data)
    {
        channels[cid] = Channel(data);
    }

    Channel add_channel(string cid, string name)
    {
        channels[cid] = Channel(([
            "id": cid,
            "name": name,
        ]));

        return channels[cid];
    }

    string version()
    {
        return "1.0.999999";
    }
}

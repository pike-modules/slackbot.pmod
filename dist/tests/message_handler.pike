import "..";
import ".";
inherit Testing.TestCase;

#include "macros.h"

private Testing.Mock _message_handler;

void tear_up()
{
    array vars = ({
        ({ "global", "debug", "debug", Config.Bool, false }),
    });

    TestBot.TestBot bot = TestBot.TestBot("tests/message_handler.json", vars);

    add_constant("BOT", bot);

    string handler_file = "message_handler.pike";
    _message_handler = Testing.Mock(handler_file);
    _message_handler->set_header("import \"pike-modules\";");

    bot->config->read();
}

void tear_down()
{
    _message_handler = 0;
    add_constant("BOT");
}

bool test_create()
{
    _message_handler->mock("commands", "multiset(string)", "protected",
        "multiset(string) commands = (< \"test_cmd\" >);");

    ASSERT_EQUAL(sizeof(_message_handler()->commands), 1);

    return true;
}

bool test_apply()
{
    multiset(string) commands = (< "test_cmd", "test_cmd2" >);
    _message_handler->mock("commands", "multiset(string)", "protected",
        "multiset(string) commands = (< \"test_cmd\", \"test_cmd2\" >);");

    ASSERT_EQUAL(_message_handler()->commands, commands);
    ASSERT_TRUE(_message_handler()->applies(([
        "text": "test_cmd"
    ])));
    ASSERT_FALSE(_message_handler()->applies(([
        "text": "test_cmd3"
    ])));
    ASSERT_FALSE(_message_handler()->applies(UNDEFINED));
    ASSERT_FALSE(_message_handler()->applies(([ ])));

    return true;
}

bool test_get_commands()
{
    array(string) commands = ({ "test_cmd", "test_cmd_2" });
    _message_handler->mock("commands", "multiset(string)", "protected",
        "multiset(string) commands = (< \"test_cmd\", \"test_cmd_2\" >);");
    _message_handler->mock("name", "constant", "",
        "constant name = \"testname\";");

    ASSERT_EQUAL(_message_handler()->get_commands(), ([ "testname" : commands ]));

    return true;
}

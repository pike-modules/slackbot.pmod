import ".";
inherit Testing.TestCase;

#include "macros.h"
#include "utils.h"

private Testing.Mock _message_handler;

void tear_up()
{
    array vars = ({
        ({ "global", "debug", "debug", Config.Bool, false }),
    });

    TestBot.TestBot _bot = TestBot.TestBot("tests/version.json", vars);
    add_constant("BOT", _bot);

    _bot->add_user("U1", "User1");
    _bot->add_user("U2", "User2");
    _bot->add_user("U3", "User3");
    string handler_file = "message_handlers/version.pike";
    _message_handler = Testing.Mock(handler_file);

    _bot->config->read();
}

void tear_down()
{
    _message_handler = 0;
    add_constant("BOT");
}

bool test_apply()
{
    ASSERT_TRUE(_message_handler()->applies(([
        "text": "!version"
    ])));
    ASSERT_FALSE(_message_handler()->applies(UNDEFINED));
    ASSERT_FALSE(_message_handler()->applies(([ ])));

    return true;
}

bool test_get_commands()
{
    array(string) commands = ({ "!version" });
    ASSERT_EQUAL(_message_handler()->get_commands(), ([ "version" : commands ]));

    return true;
}

bool test_get_action()
{
    array(mapping(string:string|int)) resp =
        _message_handler()->get_action(([ "text": "!version", "user": "U1" ]));
    ASSERT_EQUAL(get_resp_text(resp), "1.0.999999");
}

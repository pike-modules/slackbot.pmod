constant name = "admin";

bool applies(mapping(string:mixed) data)
{
    if (stringp(data?->text) && has_prefix(data->text, "!admin"))
    {
        if (!sizeof(BOT->query("admins")))
        {
            BOT->get_section("global")["admins"] =
                BOT->query("admins") + ({ data->user });
            BOT->write_cfg();
            data->admin = 1;
            data->admin_granted = 1;
            return true;
        }

        if (has_value(BOT->query("admins"), data->user))
            data->admin = 1;
        else
            data->admin = -1;

        return true;
    }

    return false;
}

array(mapping(string:mixed))
get_response(mapping(string:mixed) data, array(object) modules)
{
    array(string) cmd = data->text / " ";

    if (data->admin_granted == 1)
    {
        return ({ Utils.simple_response(data, "Admin rights granted.") });
    }
    else if (data->admin == -1)
    {
        return ({ Utils.simple_response(data, "There can be only one! "
                    "http://paradisefoundaround.com/wp-content/uploads/"
                    "highlander_there_can_be_only_one_quote.jpg?74f3cb") });
    }
    else if (sizeof(cmd) >= 2)
    {
        string response;
        switch (cmd[1])
        {
            case "modules":
            case "ls":
                response =
                    sprintf("Available modules:\n%{ - %s\n%}", modules->name);
                break;

            case "admins":
                response = "TBD";
                break;

            case "moderators":
                response = "TBD";
                break;

            case "help":
                response = sprintf("Available commands:\n - modules\n - ls\n"
                        " - admins\n - moderators\n - help");
                break;

            default:
                if (has_value(modules->name, cmd[1]))
                {
                    object obj = filter(modules, lambda(object x) {
                            return x->name == cmd[1];
                        })[0];

                    return obj->admin(data);
                }
                else
                {
                    response =
                        sprintf("There is no such module or command: %O", cmd[1]);
                }

                break;
        }

        return ({ Utils.simple_response(data, response) });
    }

    return ({ Utils.simple_response(data, "Disturbance in the Force I sence.\n"
                "Dark side is getting stronger in you I feel.") });
}

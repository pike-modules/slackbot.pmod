mapping(string:string|int)
simple_response(mapping(string:string|int) data, string response)
{
    return ([
            "type": "message",
            "params": ([
                "channel": data->channel,
                "text": response,
            ]),
    ]);
}

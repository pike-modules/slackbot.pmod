inherit "../message_handler";

constant name = "suchar";
constant priority = 0;

protected multiset(string) commands = (< "!suchar" >);

class Suchar(string title, string text, string image_url, string url)
{
    string dump()
    {
        string fallback =
            sprintf("%s%s", title || "Suchar!", text ? "\n" + text : "");
        mapping(string:string) ret = ([
            "fallback": fallback,
            "thumb_url": "http://wars-stars.pl/files/stars/"
                            "46d2257f83c43f7fc7512db92b0f6c77eb6d6b9dl.jpg",
        ]);

        if (title)
        {
            ret["title"] = title;
            if (url)
                ret["title_link"] = url;
        }

        if (text)
            ret["text"] = text;

        if (image_url)
            ret["image_url"] = image_url;

        return Standards.JSON.encode(({ ret }));
    }

    string encode_json()
    {
        return Standards.JSON.encode(([
            "title": title,
            "text": text,
            "image_url": image_url,
            "url": url
        ]));
    }
}

private mapping(string:array(Suchar)) suchars = ([ ]);
private string sucharry = "https://sucharry.pl/";
private string kretyn = "http://kretyn.com/";
private mapping(string:int) pages_fetched = ([ ]);
private string backup_file;

mapping(string:mixed) get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([
         "api": "chat.postMessage",
         "type": "api",
    ]);

    ret["params"] = ([
        "channel": data["channel"],
        "as_user": true,
        "parse": "full",
        "username": BOT->get_name(),
    ]);

    Suchar suchar = get_suchar();

    if (!suchar)
        return UNDEFINED;

    ret["params"]["attachments"] = suchar.dump();

    return ret;
}

private Suchar get_suchar()
{
    array(Suchar) sucharki = values(suchars)*({ });
    if (!suchars || !sizeof(sucharki))
        return UNDEFINED;

    int r = random(sizeof(sucharki));

    return sucharki[r];
}
private array(Parser.XML.SloppyDOM.Node) get_nodes(string url)
{
    Protocols.HTTP.Query q = Protocols.HTTP.get_url(url);

    if (q.status != 200 || !strlen(q.data() || ""))
        return 0;

    Parser.XML.SloppyDOM.Document dom = Parser.XML.SloppyDOM.parse(q.data());
    return dom->get_descendant_nodes();
}

private void save()
{
    string sucharki = Standards.JSON.encode(suchars);
    string pages = Standards.JSON.encode(pages_fetched);
    string data = Standards.JSON.encode(([
        "suchars": sucharki,
        "pages": pages
    ]));

    Stdio.write_file(backup_file, string_to_utf8(data), 0644);
}

private void fetch_kretyn()
{
    int get_pages()
    {
        array(Parser.XML.SloppyDOM.Node) nodes = get_nodes(kretyn + "najnowsze");

        if (!nodes)
            return 0;

        array(mixed) r;
        foreach (nodes; int index; Parser.XML.SloppyDOM.Node node)
        {
            mixed e = catch {
                r = node->simple_path("body//a[@class=\"paginator lpaginator\"]");
            };

            if (!e && arrayp(r) && sizeof(r))
            {
                array(string) pages = Array.flatten(r->get_descendant_nodes()->node_value);
                pages = Array.sort_array(
                        pages,
                        lambda(string x, string y) {return (int)x > (int)y;});
                return (int)(pages[-1]);
            }
        }

        return 0;
    };

    int pages = get_pages();

    if (pages_fetched[kretyn] >= pages)
        return;

    if (pages)
        suchars[kretyn] = ({ });

    void fetch_page(int page)
    {
        string page_url = sprintf("%snajnowsze,page-%d", kretyn, page);
        array(Parser.XML.SloppyDOM.Node) nodes = get_nodes(page_url);
        if (!nodes)
            return;

        int j = 0;
        Parser.XML.SloppyDOM.Node html = nodes[j];
        while (j < sizeof(nodes) && html.node_name != "html")
            html = nodes[++j];

        string path = "body//div[@class=\"cytat\"]";
        array(array(string)) suchar_text =
            html->simple_path(path)->get_descendant_nodes()->node_value;
        array(array(string)) suchar_title =
            html->simple_path(path)->get_attribute("id");

        Charset.Decoder dec = Charset.decoder("utf-8");
        for (j = 0; j < sizeof(suchar_text); j++)
        {
            mixed e = catch {
                string title = dec.feed((string)(suchar_title[j][1..])).drain();
                string body = dec.feed(suchar_text[j]*"").drain();
                suchars[kretyn] += ({ Suchar(title, body, 0, kretyn + title) });
            };
        }

        pages_fetched[kretyn] = page;
        save();

        if (page < pages)
        {
            call_out(fetch_page, 0, ++page);
        }
        else
        {
            werror("[suchar] fetched suchars from kretyn.com\n");
        }
    };

    call_out(fetch_page, 0, 1);
    call_out(fetch_kretyn, 7200);
}

private void fetch_sucharry()
{
    int get_pages()
    {
        array(Parser.XML.SloppyDOM.Node) nodes = get_nodes(sucharry);

        if (!nodes)
            return 0;

        array(mixed) r;
        foreach (nodes; int index; Parser.XML.SloppyDOM.Node node)
        {
            mixed e = catch {
                r = node->simple_path("body//div[@id=\"paginator\"]")->get_attribute("data");
            };

            if (!e && arrayp(r) && sizeof(r) == 1 && stringp(r[0]) && (int)r[0])
                return (int)r[0];
        }

        return 0;
    };

    int pages = get_pages();

    if (pages_fetched[sucharry] >= pages)
        return;

    if (pages)
        suchars[sucharry] = ({ });

    void fetch_page(int page)
    {
        string page_url = sprintf("%s/strona/%d", sucharry, page);

        array(Parser.XML.SloppyDOM.Node) nodes = get_nodes(page_url);
        if (!nodes)
            return;

        int j = 0;
        Parser.XML.SloppyDOM.Node html = nodes[j];
        while (j < sizeof(nodes) && html.node_name != "html")
            html = nodes[++j];

        string path = "body//div[@class=\"suchar\"]";
        nodes = html->simple_path(path);

        path = "h3/a";
        array(string) suchar_title =
            nodes->simple_path(path)->get_descendant_nodes()->node_value*({ })*({ });
        array(string) suchar_url =
            nodes->simple_path(path)->get_attribute("href")*({ });

        path = "a[@class=\"link\"]/img";
        array(string) suchar_text = allocate(sizeof(suchar_title));
        array(string) suchar_image = allocate(sizeof(suchar_title));
        foreach (nodes; int index; Parser.XML.SloppyDOM.Node n)
        {
            array(mixed) data = n->simple_path(path);

            if (data && sizeof(data) == 1 && data[0].node_name == "img")
            {
                string imgsrc = data[0]->get_attribute("src");
                suchar_image[index] = sucharry + imgsrc;
            }
            else
            {
                data = n->simple_path("a[@class=\"link\"]");
                if (!sizeof(data))
                    continue;

                string body = data[0]->get_descendant_nodes()[0]->node_value;
                data = n->simple_path("a[@class=\"showtxt\"]");
                if (sizeof(data))
                {
                    body =
                        sprintf("%s\n\n%s", body, data[0]->get_attribute("data-txt"));
                }

                suchar_text[index] = body;
            }
        }

        Charset.Decoder dec = Charset.decoder("utf-8");
        for (j = 0; j < sizeof(suchar_text); j++)
        {
            catch
            {
                string title = dec.feed(suchar_title[j]).drain();
                string body = suchar_text[j] && dec.feed(suchar_text[j]).drain();
                string image = suchar_image[j] && dec.feed(suchar_image[j]).drain();
                string url = dec.feed(suchar_url[j]).drain();
                suchars[sucharry] += ({ Suchar(title, body, image, url) });
            };
        }

        pages_fetched[sucharry] = page;
        save();

        if (page < pages)
        {
            call_out(fetch_page, 0, ++page);
        }
        else
        {
            werror("[suchar] fetched suchars from sucharry.pl\n");
        }
    };

    call_out(fetch_page, 0, 1);

    werror("[suchar] fetched suchars from sucharry.pl\n");
    call_out(fetch_sucharry, 7200);
}

void init()
{
    backup_file = combine_path(BOT->query("runtime"), "fetched_suchars.json");

    if (Stdio.exist(backup_file))
    {
        string backup = Stdio.read_file(backup_file);
        if (backup && strlen(backup))
        {
            mapping(string:string) data =
                Standards.JSON.decode(utf8_to_string(backup));

            pages_fetched = Standards.JSON.decode(data["pages"]);

            mapping(string:array(mapping(string:mixed))) sucharki =
                Standards.JSON.decode(data["suchars"]);

            foreach (sucharki; string service;
                array(mapping(string:mixed)) payload)
            {
                foreach (payload; int index; mapping(string:mixed) s)
                {
                    suchars[service] +=
                        ({
                            Suchar(s["title"], s["text"],
                                s["image_url"], s["url"])
                        });
                }
            }
            werror("[suchar] Loaded %d suchars\n", sizeof(values(suchars)*({ })));
        }
    }

    call_out(fetch_sucharry, 0);
    call_out(fetch_kretyn, 0);
}

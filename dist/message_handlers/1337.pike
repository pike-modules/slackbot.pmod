inherit "../message_handler";
import Slack.Objects;

constant name = "1337";
constant priority = 0;
constant day2str = ({
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    });

class Score(string name, int score) {}

class ScoreTable
{
    private array(Score) scores = ({ });

    private void swap(int a, int b)
    {
        Score tmp = scores[a];
        scores[a] = scores[b];
        scores[b] = tmp;
    }

    array(Score) get_scores() { return scores; }
    void set_scores(array(Score) new_scores) { scores = new_scores; }

    void add(string u)
    {
        int i = search(scores->name, u);
        if (i != -1)
        {
            Score score = scores[i];
            score->score++;
            while (i > 0 && scores[i-1]->score < score->score)
            {
                swap(i, i-1);
                i--;
            }
        }
        else
        {
            scores += ({ Score(u, 1) });
        }
    }

    ScoreTable `+(ScoreTable other)
    {
        array(Score) other_scores = other->get_scores();
        array(Score) this_scores = copy_value(scores);
        foreach (other_scores, Score score)
        {
            int i = search(this_scores->name, score->name);
            if (i != -1)
            {
                Score s = this_scores[i];
                s->score += score->score;
                while (i > 0 && this_scores[i-1]->score < s->score)
                {
                    swap(i, i-1);
                    i--;
                }
            }
            else
            {
                this_scores += ({ score });
            }
        }

        this_scores = Array.sort_array(this_scores, lambda(Score a, Score b) {
                return a->score < b->score;
        });

        ScoreTable ret = ScoreTable();
        ret->set_scores(this_scores);

        return ret;
    }

    string _sprintf()
    {
        string ret = "ScoreTable(";
        foreach (scores, Score score)
            ret += sprintf(
                    "%s:%d,",
                    score->name,
                    score->score
                   );
        return ret + ")";
    }
}

class Week
{
    private array(string) days = allocate(7);
    private Year year;
    private int week;

    void create(int w, Year y)
    {
        week = w;
        year = y;
    }

    bool is_empty() { return `+(@days) == 0; }

    bool attempt(int d, string u)
    {
        if (d > year->get_num_of_week_days()-1 || d < 0)
            error("InvalidValue: Expected 0..%d, got: %d",
                    year->get_num_of_week_days()-1, d);

        if (days[d])
            return false;

        days[d] = u;

        return true;
    }

    void set(int d, string u)
    {
        if (d >= 7 || d < 0)
            error("InvalidValue: Expected 0..6, got: %d", d);

        days[d] = u;
    }

    array(Score) get_scores()
    {
        ScoreTable score_table = ScoreTable();
        foreach (days; int index; string u)
        {
            if (index >= year->get_num_of_week_days())
                break;

            if (u)
                score_table->add(u);
        }

        return score_table->get_scores();
    }

    string describe()
    {
        string ret = sprintf("*Week %d*, %s:\n\n```", week, year->to_string());

        array(Score) scores = get_scores();
        foreach (scores; int index; Score score)
        {
            ret += sprintf("%2d. %-10s %d\n", index+1,
                    BOT->get_user(score->name)?->name, score->score);
        }

        ret += "\n";

        foreach (days; int index; string u)
        {
            if (index >= year->get_num_of_week_days())
                break;

            ret += sprintf("%-10s %10s\n", day2str[index],
                    BOT->get_user(u)?->name || "");
        }

        return ret + "```";
    }

    string `[](int day)
    {
        if (day >= 7 || day < 0)
            error("InvalidValue: Expected 0..6, got: %d", day);

        return days[day];
    }

    string encode_json()
    {
        return Standards.JSON.encode(([
                    "week": week,
                    "days": days,
        ]));
    }

    void load(mapping data)
    {
        week = data["week"];
        days = data["days"];
    }

    string to_string() { return (string)week; }
    int to_int() { return week; }

    string _sprintf()
    {
        return sprintf("1337_week(%d, y: %d)", week, year->to_int());
    }
}

class Year
{
    private mapping(int:Week) weeks = ([ ]);
    private int year;
    private Channel channel;

    void create(int y, Channel c) {

        year = y;
        channel = c;
    }

    bool is_empty() { return sizeof(weeks) == 0; }

    int get_num_of_week_days() { return channel->get_num_of_week_days(); }

    Week add(int w)
    {
        if (!weeks[w])
            weeks[w] = Week(w, this);

        return weeks[w];
    }

    Week `[](int w)
    {
        return weeks[w] || Week(w, this);
    }

    string to_string() { return (string)year; }
    int to_int() { return year; }

    string encode_json()
    {
        mapping(string:Week) w = ([ ]);
        foreach (weeks; int index; Week ww)
            w[(string)index] = ww;

        return Standards.JSON.encode(([
                    "weeks": w,
                    "year": year
        ]));
    }

    void load(mapping data)
    {
        foreach (data->weeks; string w; mapping wdata)
            add((int)w)->load(wdata);
    }

    ScoreTable get_score_table()
    {
        ScoreTable score_table = ScoreTable();
        foreach (weeks; int index; Week week)
        {
            for (int i = 0; i < get_num_of_week_days(); i++)
            {
                if (week[i])
                    score_table->add(week[i]);
            }
        }
        return score_table;
    }

    string describe()
    {
        string ret = sprintf("*Year %d*:\n\n```", year);

        ScoreTable score_table = this->get_score_table();

        foreach (score_table->get_scores(); int index; Score score)
        {
            ret += sprintf("%2d. %-10s %d\n", index+1,
                    BOT->get_user(score->name)?->name, score->score);
        }

        return ret + "```";
    }

    string _sprintf()
    {
        return sprintf("1337_year(%d, #weeks: %d)", year, sizeof(weeks));
    }
}

class Channel
{
    private mapping(int:Year) years = ([ ]);
    private string name;
    private int num_of_week_days = 7;
    int offset = 1;

    void create(string n) { name = n; }

    Year add(int y)
    {
        if (!years[y])
            years[y] = Year(y, this);

        return years[y];
    }

    Year `[](int y)
    {
        return years[y] || Year(y, this);
    }

    int get_num_of_week_days() { return num_of_week_days; }

    bool set_num_of_week_days(int d)
    {
        if (d > 0 && d <=7 )
        {
            num_of_week_days = d;
            return true;
        }

        return false;
    }

    string encode_json()
    {
        mapping(string:Year) y = ([ ]);
        foreach (years; int index; Year yy)
            y[(string)index] = yy;

        return Standards.JSON.encode(([
                    "years": y,
                    "num_of_week_days": num_of_week_days,
                    "offset": offset,
        ]));
    }

    void load(mapping data)
    {
        foreach (data->years; string y; mapping ydata)
            add((int)y)->load(ydata);

        num_of_week_days = data["num_of_week_days"] || 7;
        offset = data["offset"] || 1;
    }

    string describe_all()
    {
        ScoreTable scores = ScoreTable();

        foreach (values(years), Year y)
        {
            scores = scores + y->get_score_table();
        }

        string ret = sprintf("*Hall of fame*:\n\n```");

        foreach (scores->get_scores(); int index; Score score)
        {
            ret += sprintf("%2d. %-10s %d\n", index+1,
                    BOT->get_user(score->name)?->name, score->score);
        }

        return ret + "```";
    }

    string _sprintf()
    {
        return sprintf("1337_channel(%s)", name);
    }
}

protected multiset(string) commands = (< "13:37", "!leetscore" >);
private mapping(string:Channel) channels = ([ ]);
private mapping(string:int) last_hit_given_at = ([ ]);
private string backup_file;

array(mapping(string:string|int)) admin(mapping(string:string|int) data)
{
    array(string) cmd = data->text / " ";
    string resp = "Ayy";

    if (cmd[2] == "set")
    {
#define DAY 86400
        mapping(string:string|int) ts;
        if (cmd[3] == "today" || sizeof(cmd) == 4)
            ts = Calendar.Day("unix", time())->datetime();
        else if(cmd[3] == "yesterday")
            ts = Calendar.Day("unix", time()-DAY)->datetime();
        else if (sscanf(cmd[3], "-%d", int days))
            ts = Calendar.Day("unix", time()-days*DAY)->datetime();
        else if (mixed parsed = Calendar.parse("%a.%M.%Y", cmd[3]))
            ts = parsed->datetime();
        else
            return ({ Utils.simple_response(data, "Wrong time format.") });
#undef DAY

        int wday = (!ts->week_day ? 7 : ts->week_day) - 1;
        string ch = data->channel;

        array(int) yw = make_tree(ch, ts->year, ts->week, wday);
        int year = yw[0];
        int week = yw[1];

        User user = BOT->get_user_by_name(cmd[4]);

        if (user)
        {
            channels[ch][year][week]->set(wday, user->id);
            save();
            resp = "Done";
        }
        else
        {
            resp = sprintf("No such user: %O", cmd[4]);
        }
    }
    else if (cmd[2] == "config")
    {
        mapping(string:string|int) ts =
            Calendar.Day("unix", time())->datetime();
        int wday = (!ts->week_day ? 7 : ts->week_day) - 1;

        make_tree(data->channel, ts->year, ts->week, wday);
        if (sizeof(cmd) < 5)
            return ({
                Utils.simple_response(
                    data,
                    "Missing some arguments to `config`."
                )
            });

        if (cmd[3] == "num_of_week_days")
        {
            if (channels[data->channel]->set_num_of_week_days((int)cmd[4]))
                resp = "Done";
            else
                return ({ Utils.simple_response(data, "Something went wrong.") });
        }
        else if (cmd[3] == "offset")
        {
            channels[data->channel]->offset = (int)cmd[4];
            resp = "Done";
        }

        save();
    }

    return ({ Utils.simple_response(data, resp) });
}

bool applies(mapping(string:mixed) data)
{
    return data?->text
        && (data->text == "13:37" || has_prefix(data->text, "!leetscore"))
        && !data?->bot_id;
}

mapping get_action(mapping(string:mixed) data)
{
    if (has_prefix(data?->text, "!leetscore"))
        return get_score(data);
    else
        return check_if_hit(data);
}

private inline Channel get_channel(string ch)
{
    return channels[ch] || Channel(ch);
}

private array(int) make_tree(string ch, int year, int week, int wday)
{
    if (undefinedp(channels[ch]))
       channels[ch] = Channel(ch);

    if (channels[ch][year]->is_empty() && wday && week >= 53)
    {
        week -= 1;
        year -= 1;
    }

    if (channels[ch][year]->is_empty())
        channels[ch]->add(year);

    if (channels[ch][year][week]->is_empty())
        channels[ch][year]->add(week);

    return ({ year, week });
}

private mapping check_if_hit(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    User user = BOT->get_user(data["user"]);

    mapping ts = localtime((int)data->ts);
    mapping datetime = Calendar.Week("unix", (int)data->ts)->datetime();
    int offset = get_channel(data->channel)->offset;

    if (ts->hour + offset == 13 && ts->min == 37)
    {
        ret["params"]["text"] = "13:37 indeed, @" + user?->name;

        int wday = (!ts->wday ? 7 : ts->wday) - 1;
        string ch = data->channel;

        array(int) yw = make_tree(ch, datetime->year, datetime->week, wday);
        int year = yw[0];
        int week = yw[1];

        if (channels[ch][year][week]->attempt(wday, data->user))
        {
            ret["params"]["text"] += " - and you were fastest gun today";
            save();
        }
    }
    else if (ts->hour + offset == 13 && (< 38, 36 >)[ts->min])
    {
        if (ts->sec == 59)
        {
            ret["params"]["text"] = "You fapped @" + user?->name + " :D";
        }
        else
        {
            ret["params"]["text"] = "So close, you'll make it next time @"
                + user?->name;
        }
    }
    else
        ret["params"]["text"] = "Nope, not even close @" + user?->name;

    return ret;
}

private mapping get_score(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    array(string) cmds = data->text/" ";

    if (sizeof(cmds) == 1)
        cmds = cmds + ({ "current" });

    mapping ts = localtime((int)data->ts);
    mapping datetime = Calendar.Week("unix", (int)data->ts)->datetime();

    int year = datetime->year;
    int week = datetime->week;
    int wday = (!ts->wday ? 7 : ts->wday) - 1;

    switch (cmds[1])
    {
        case "current":
            if (week == 54)
            {
                week -= 1;
                year -= 1;
            }
            Channel ch = get_channel(data->channel);
            ret["params"]["text"] = ch[year][week]->describe();
            break;
        case "week":
            if (sizeof(cmds) <= 2)
            {
                ret["params"]["text"] = "Missing week number 1..53";
                break;
            }

            week = (int)cmds[2];

            if (week < 1 || week > 53)
            {
                ret["params"]["text"] = "Week number must be 1..53";
            }

            if (sizeof(cmds) >= 4)
                year = (int)cmds[3];

            ret["params"]["text"] =
                get_channel(data->channel)[year][week]->describe();
            break;
        case "year":
            if (sizeof(cmds) <= 2)
            {
                ret["params"]["text"] = sprintf("Missing year (<= %d)", year);
                break;
            }

            if ((int)cmds[2] > year)
            {
                ret["params"]["text"] = sprintf("Year must be <= %d", year);
            }

            year = (int)cmds[2];

            ret["params"]["text"] =
                get_channel(data->channel)[year]->describe();
            break;
        case "alltime":
        case "halloffame":
            ret["params"]["text"] =
                get_channel(data->channel)->describe_all();
            break;
        default:
            if (sizeof(cmds) > 1 && (int)(cmds[1]) < 54 && (int)(cmds[1]) > 0)
            {
                week = (int)cmds[1];
                if (sizeof(cmds) == 3)
                    year = (int)cmds[2];

                ret["params"]["text"] =
                    get_channel(data->channel)[year][week]->describe();
            }
            break;

    }

    return ret;
}

private void save()
{
    string data = Standards.JSON.encode(channels);

    Stdio.write_file(backup_file, string_to_utf8(data), 0644);
}

void init()
{
    backup_file = combine_path(BOT->query("runtime"), "1337_scores.json");

    if (Stdio.exist(backup_file))
    {
        string backup = Stdio.read_file(backup_file);
        if (backup && strlen(backup))
        {
            mapping data = Standards.JSON.decode(utf8_to_string(backup));
            foreach (data; string ch; mapping chdata)
            {
                channels[ch] = Channel(ch);
                channels[ch]->load(chdata);
            }

            werror("[1337] Loaded scores\n");
        }
    }
}

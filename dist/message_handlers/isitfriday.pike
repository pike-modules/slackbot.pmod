inherit "../message_handler";

constant name = "isitfriday";
constant priority = 0;

protected multiset(string) commands = (< "is it friday" >);

bool applies(mapping(string:mixed) data)
{
    return has_value(lower_case(data?->text || ""), "is it friday");
}

mapping get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    mapping ts = gmtime((int)data->ts);

    if (ts->wday == 5)
        ret["params"]["text"] = "Yes, it is.";
    else
        ret["params"]["text"] = "Nope, it's not. See for yourself: "
                                "http://isitfriday.org";

    return ret;
}


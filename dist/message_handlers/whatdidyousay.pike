inherit "../message_handler";
import Slack.Objects;

constant name = "whatdidyousay";
constant priority = -100;

private array(string) replies = ({
    "What did you say? http://gph.is/1ITgjWY",
    "...",
    "Please speak up, I can't hear you",
    "Yes!",
    "No!",
    "Why?",
    "Go away!",
    "Leave me alone!",
    "I don't know",
        });

bool applies(mapping(string:mixed) data)
{
    return !data->was_handled && data?->text
            && has_prefix(data->text, sprintf("<@%s>", BOT->get_self()->id));
}

mapping get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    User user = BOT->get_user(data->user);

    array(string) _replies = query("replies");
    if (!sizeof(_replies || ({ })))
        _replies = replies;

    ret["params"]["text"] = sprintf("<@%s|%s>: %s",
                                user->id,
                                user->name,
                                _replies[random(sizeof(_replies))]);

    return ret;
}

void define_variables()
{
    ::define_variables();
    modvar("replies", "Array of replies that will be used",
            Config.Array, ({ }));
}

inherit "../message_handler";
import Slack.Objects;

constant name = "slap";
constant priority = 0;

private Regexp.PCRE.Widestring re;

bool applies(mapping(string:mixed) data)
{
    return data?->text && re.match(data->text) && !data->bot_id;
}

mapping(string:mixed) get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([
         "api": "chat.postMessage",
         "type": "api",
    ]);

    ret["params"] = ([
        "channel": data["channel"],
        "as_user": true,
        "parse": "full",
        "username": BOT->get_name(),
    ]);

    User user = BOT->get_user(data->user);

    string who = re.split(data->text)[0];

    ret["params"]["text"] =
        sprintf("@%s slaps @%s around a bit with a large :trout:", user->name, who);

    if (who == "everyone")
    {
        array(string) slap_gifs = ({
                "https://media1.giphy.com/media/Uun2kN00aMp8c/giphy.gif?cid=6104955e5d4009e4315a542e77fdd62c&rid=giphy.gif",
                "https://media.giphy.com/media/vJhJq3SISV8jK/giphy.gif"
        });
        ret["params"]["attachments"] = string_to_utf8(Standards.JSON.encode(({
                ([
                    "text": "slap",
                    "image_url": random(slap_gifs)
                ])
                })));
    }

    return ret;
}

void create()
{
    ::create();
    re = Regexp.PCRE.Widestring("^!slap ([A-Za-z0-9_]+)$");
}

mapping(string:array(string)) get_commands()
{
    return ([ name: ({ "!slap <channel|here|everyone|username>" }) ]);
}

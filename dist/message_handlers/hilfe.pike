inherit "../message_handler";

constant name = "hilfe";
constant priority = 0;

protected multiset(string) commands = (< "!help", "!hilfe", "!pomocy", "!pomoc" >);

mapping(string:mixed) get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    ret["params"]["text"] = "";

    foreach (BOT->get_commands(data->channel); string module; array(string) commands)
    {
        if (!sizeof(commands))
            continue;

        ret["params"]["text"] += sprintf("*%s*:\n%{ - %s\n%}\n", module, commands);
    }

    return ret;
}

inherit "../message_handler";

constant name = "version";
constant priority = 0;

protected multiset(string) commands = (< "!version" >);

array(mapping(string:mixed)) get_action(mapping(string:mixed) data)
{
    return ({ Utils.simple_response(data, BOT->version()) });
}

inherit "../message_handler";

constant name = "jira";
constant priority = 0;

private Regexp.PCRE.Widestring re;

bool applies(mapping(string:mixed) data)
{
    return data?->text && re.match(data->text) && !data->bot_id;
}

mapping(string:mixed) get_action(mapping(string:mixed) data)
{
    array(string) matches = ({ });
    re.matchall(data->text, lambda(mixed s) { matches += ({ s[0] }); });

    string issue_url = "https://bugs.opera.com/browse/";

    mapping(string:mixed) ret = ([
            "params": ([
                "channel": data->channel,
            ]),
            "api": "chat.postMessage",
            "type": "api",
            "as_user": true,
            "parse": "full",
            "username": BOT->get_name(),
    ]);

    array(mapping(string:mixed)) attachments = ({ });

    foreach (matches; int index; string id)
    {
        mapping issue = query_jira(id);

        mapping action = ([ ]);
        action["title"] =
            sprintf("%s: %s (%s)", id, issue->summary, issue->status);
        action["title_link"] = issue_url + id;
        action["fallback"] =
            sprintf("%s\n%s", action["title"], action["title_link"]);
        action["color"] =
            sprintf("#%2x%2x%2x", random(256), random(256), random(256));

        attachments += ({ action });
    }

    ret["params"]["attachments"] = string_to_utf8(Standards.JSON.encode(attachments));

    return ret;
}

private mapping(string:mixed)
query_jira(string issue)
{
    Protocols.HTTP.Query q = Protocols.HTTP.get_url(query("url")
        + "issue/" + issue, UNDEFINED, ([ "Authorization": get_auth() ]));

    if (q.status != 200)
        return UNDEFINED;

    mapping resp = Standards.JSON.decode_utf8(q.data());

    mapping ret = ([ ]);

    ret->summary = resp?->fields->summary;
    ret->status = resp?->fields->status->name;

    return ret;
}

private string get_auth()
{
    string auth = sprintf("%s:%s", query("user"), query("password"));
    return "Basic " + MIME.encode_base64(auth, 1);
}

void create()
{
    ::create();

    re = Regexp.PCRE.Widestring("([A-Z]+-[0-9]+)");
}

void define_variables()
{
    ::define_variables();
    modvar("user", "Username for JIRA bug tracker",
            Config.String);
    modvar("password", "Password for JIRA bug tracker",
            Config.String);
    modvar("url", "REST API url for JIRA bug tracker",
            Config.String);
}

inherit "../message_handler";

constant name = "random quote";
constant priority = 0;

protected multiset(string) commands = (< "!quote random",
                                         "!quote",
                                         "!quote add",
                                         "!quote list",
                                         "!quote search" >);

private string backup_file;
private mapping(string:mapping(string:string)) quotes = ([ ]);
private Regexp.PCRE.Widestring origin;
private Regexp.PCRE.Widestring img;
private Regexp.PCRE.Widestring commandre;

bool applies(mapping(string:mixed) data)
{
    return data?->text && commandre.match(data->text);
}

mapping(string:mixed) get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([
        "type": "message",
        "params": ([
            "channel": data?->channel,
        ]),
    ]);

    if (data->text == "!quote random")
        ret["params"]["text"] = get_random_quote();
    else if (has_prefix(data->text, "!quote add"))
        ret["params"]["text"] = add_quote(data->text);
    else if (has_prefix(data->text, "!quote list"))
        ret["params"]["text"] = list_quotes();
    else if (has_prefix(data->text, "!quote search"))
    {
        string pattern = data->text - "!quote search ";
        ret["params"]["text"] = search_quote(pattern);
    }
    else
    {
        string id = data->text - "!quote ";
        ret["params"]["text"] = get_quote(id);
    }

    return ret;
}

private string get_quote(string id)
{
    if (!has_index(quotes, id))
        return "Unknown quote id";

    mapping(string:string) quote = quotes[id];
    string ret = quote["quote"];
    if (quote["origin"])
        ret = sprintf("*%s*\n%s", quote["origin"], ret);
    if (quote["img"])
        ret = sprintf("%s\n%s", ret, quote["img"]);

    ret = sprintf("%s. %s", id, ret);

    return ret;
}

private string list_quotes()
{
    array(string) ret = ({ });
    array(string) quote_ids = Array.sort_array(
            indices(quotes),
            lambda(string a, string b) { return (int)a > (int)b; }
    );
    foreach (quote_ids, string index)
        ret += ({ get_quote(index) });

    if (sizeof(ret))
    {
        return ret*"\n";
    }
    else
    {
        return "Unable to find any quote.";
    }
}

private string search_quote(string pattern)
{
    array(string) ret = ({ });
    foreach (quotes; string index; mapping(string:string) quote)
    {
        if (has_value(lower_case(quote["quote"]), pattern)
            || quote["origin"] && has_value(lower_case(quote["origin"]), pattern))
        {
            ret += ({ get_quote(index) });
        }
    }

    if (sizeof(ret))
    {
        return Array.sort(ret)*"\n";
    }
    else
    {
        return "Unable to find any quote matching given pattern.";
    }
}

private string get_random_quote()
{
    if (!sizeof(quotes))
        return "No quotes at this moment. Consider adding some with `!quote add`";

    array(string) quotes_ids = indices(quotes);
    string random_id = quotes_ids[random(sizeof(quotes_ids))];

    return get_quote(random_id);
}

private string add_quote(string text)
{
    text = text[sizeof("!quote add")..];
    if (!strlen(text))
        return "Usage `!quote add {origin:random quote} This is my random quote`";

    mapping(string:string) new_quote = ([ ]);

    if (origin.match(text))
    {
        array(string) matches = ({ });
        origin.matchall(text, lambda(mixed s) { matches = s; });
        new_quote["origin"] = matches[2];
        text -= matches[0];
        if (strlen(text) < 2)
           return "Text of quote is to short or lack of it";
    }

    if (img.match(text))
    {
        array(string) matches = ({ });
        img.matchall(text, lambda(mixed s) { matches = s; });
        new_quote["img"] = matches[2];
        text -= matches[0];
        if (strlen(text) < 2)
           return "Text of quote is to short or lack of it";
    }

    new_quote["quote"] = text;
    string new_id = "1";
    if (sizeof(quotes))
    {
        new_id = Array.sort_array(indices(quotes),
                lambda(string a, string b) { return (int)a > (int)b; })[-1];
        new_id = (string)((int)new_id+1);
    }

    quotes[new_id] = new_quote;
    save();

    return get_quote(new_id);
}

private void save()
{
    string data = Standards.JSON.encode(quotes);

    Stdio.write_file(backup_file, string_to_utf8(data), 0644);
}

void init()
{
    origin = Regexp.PCRE.Widestring("({origin:)([^}]+)}");
    img = Regexp.PCRE.Widestring("({img:)([^}]+)}");
    commandre =
        Regexp.PCRE.Widestring("^!(quote random$|quote add [^$]+$|quote search [^$]+$|quote list$|quote [0-9]+$)");
    backup_file = combine_path(BOT->query("runtime"), query("quote file"));
    if (Stdio.exist(backup_file))
    {
        string backup = Stdio.read_file(backup_file);
        if (backup && strlen(backup))
        {
            quotes = Standards.JSON.decode(utf8_to_string(backup));
            werror("[random quote] Loaded %d quotes\n", sizeof(quotes));
        }
    }
}

void define_variables()
{
    ::define_variables();

    modvar("quote file", "File containing quotes. Must be JSON format",
            Config.String, "randomquotes.json");
}

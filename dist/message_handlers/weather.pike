inherit "../message_handler";

constant name = "weather";
constant priority = 0;

private Regexp.PCRE.Widestring re;

private array(string) beaufort_to_english = ({
        "calm",
        "light air",
        "light breeze",
        "gentle breeze",
        "moderate breeze",
        "fresh breeze",
        "strong breeze",
        "moderate gale",
        "fresh gale",
        "strong gale",
        "storm",
        "violent storm",
        "hurricane",
        });
private mapping(int:string) symbol_to_english = ([
        1: "fair",
        2: "light clouded",
        3: "partly clouded",
        4: "clouded",
        5: "light rain and sun",
        6: "light rain, thunder and sun",
        7: "sleet and sun",
        8: "snow and sun",
        9: "light rain",
        10: "rain",
        11: "rain and thunder",
        12: "sleet",
        13: "snow",
        14: "snow and thunder",
        15: "fog",
        16: "sun, winter darkness",
        17: "light clouded, winter darkness",
        18: "light rain and sun, winter darkness",
        19: "snow and sun, winter darkness",
        20: "sleet, sun and thunder",
        21: "snow, sun and thunder",
        22: "light rain and thunder",
        23: "sleet and thunder",
        24: "drizzle, thunder and sun",
        25: "rain, thunder and sun",
        26: "light sleet, thunder and sun",
        27: "heavy sleet, thunder and sun",
        28: "light snow, thunder and sun",
        29: "heavy snow, thunder and sun",
        30: "drizzle and thunder",
        31: "light sleet and thunder",
        32: "heavy sleet and thunder",
        33: "light snow and thunder",
        34: "heavy snow and thunder",
        40: "drizzle and sun",
        41: "rain and sun",
        42: "light sleet and sun",
        43: "heavy sleet and sun",
        44: "light snow and sun",
        45: "heavy snow and sun",
        46: "drizzle",
        47: "light sleet",
        48: "heavy sleet",
        49: "light snow",
        50: "heavy snow",
        ]);

bool applies(mapping(string:mixed) data)
{
    return lower_case(data?->text) && re.match(lower_case(data->text));
}

mapping get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([
        "type": "message",
        "params": ([
            "channel": data?->channel,
        ]),
    ]);

    array(string) matches = re.split(lower_case(data?->text || ""));

    mapping(string:mixed) geodata = get_coordinates(matches[2]);

    if (!geodata || !geodata->results || !sizeof(geodata->results))
    {
        ret["params"]["text"] =
            sprintf("Unable to get geodata for %s", matches[2]);
    }
    else
    {
        ret["params"]["text"] = sprintf("%s: ",
                geodata->results[?0]?->formatted_address);
    }

    float lat = geodata->results[?0]?->geometry->location->lat;
    float lng = geodata->results[?0]?->geometry->location->lng;
    mapping(string:mixed) weatherdata = get_weatherdata(lat, lng);

    ret->params->text += sprintf("%.1fC, %s, %s",
                                    weatherdata->temperature,
                                    symbol_to_english[weatherdata->weather],
                                    beaufort_to_english[weatherdata->wind]);

    return ret;
}

private mapping(string:mixed) get_weatherdata(float lat, float lng)
{
    Standards.URI yr_api = Standards.URI(sprintf(
                "http://api.met.no/weatherapi/locationforecastLTS/1.2/"
                "?lat=%.10f;lon=%.10f", lat, lng));

    Protocols.HTTP.Query q = Protocols.HTTP.get_url(yr_api);

    if (q.status == 200)
    {
        mapping ret = ([ ]);
        Parser.XML.NSTree.NSNode n = Parser.XML.NSTree.parse_input(q.data());

        Parser.XML.NSTree.NSNode find(string|array(string) what, Parser.XML.NSTree.NSNode node)
        {
            if (stringp(what))
                what = what/"/";

            foreach (node->get_elements(), Parser.XML.NSTree.NSNode e)
            {
                if (e->get_tag_name() == what[0])
                {
                    if (sizeof(what) == 1)
                        return e;

                    Parser.XML.NSTree.NSNode ret = find(what[1..], e);
                    if (ret)
                        return ret;
                }
            }

            return UNDEFINED;
        };

        Parser.XML.NSTree.NSNode location_node =
            find("weatherdata/product/time/location", n);

        if (!location_node)
            return UNDEFINED;

        Parser.XML.NSTree.NSNode symbol =
            find("weatherdata/product/time/location/symbol", n);

        int weather_number = (int)symbol->get_attributes()?->number;
        if (weather_number > 100)
            weather_number -= 100;

        ret->temperature =
            (float)find("temperature", location_node)->get_attributes()?->value;
        ret->humidity =
            (float)find("humidity", location_node)->get_attributes()?->value;
        ret->wind =
            (int)find("windSpeed", location_node)->get_attributes()?->beaufort;
        ret->cloudiness =
            (float)find("cloudiness", location_node)->get_attributes()?->percent;
        ret->pressure =
            (float)find("pressure", location_node)->get_attributes()?->value;
        ret->weather = weather_number;

        return ret;
    }

    return UNDEFINED;
}

private mapping(string:mixed) get_coordinates(string location)
{
    Standards.URI google_api =
        Standards.URI("http://maps.googleapis.com/maps/api/geocode/json");

    mapping(string:string) params = ([
        "address" : location,
        ]);

    string data = Protocols.HTTP.get_url_data(google_api, params);

    if (!data || !strlen(data))
        return UNDEFINED;

    return Slack.parse_response(data);
}

void create()
{
    ::create();
    re = Regexp.PCRE.Widestring("(weather|pogoda) (in|w|we|at|on|na) ([^$]+)$");
}

mapping(string:array(string)) get_commands()
{
    return ([
            name: ({
                    "weather <in|at|on> <location>",
                    "pogoda <w|we|na> <gdzie>"
                })
    ]);
}

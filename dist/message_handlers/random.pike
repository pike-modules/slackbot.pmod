inherit "../message_handler";

constant name = "random";
constant priority = 0;

protected multiset(string) commands = (< "!random", "!pikerandom" >);

private Regexp.PCRE.Widestring re;

bool applies(mapping(string:mixed) data)
{
    return data?->text && re.match(data->text) && !data->bot_id;
}

mapping get_action(mapping(string:mixed) data)
{
    mapping(string:mixed) ret = ([ "type": "message" ]);
    ret["params"] = ([ "channel": data["channel"] ]);

    array(string) matches = re.split(data->text);

    if (has_value(matches[2], " "))
    {
        array(string) ranges = matches[2]/" ";
        if (matches[0] == "random")
            ret["params"]["text"] = get_random((int)ranges[0], (int)ranges[1]);
        else
            ret["params"]["text"] = get_pikerandom((int)ranges[0], (int)ranges[1]);
    }
    else
        if (matches[0] == "random")
            ret["params"]["text"] = get_random(0, (int)matches[2]);
        else
            ret["params"]["text"] = get_pikerandom(0, (int)matches[2]);

    return ret;
}

private string get_random(int start, int end)
{
    mapping(string:string) variables = ([
        "num": "1",
        "min": (string)start,
        "max": (string)end,
        "col": "1",
        "base": "10",
        "format": "plain",
        "rnd": "new"
    ]);

    Standards.URI url = Standards.URI("https://www.random.org/integers/");
    url.set_query_variables(variables);

    mapping(string:string) request_headers = ([
        "User-Agent": "",
        "Host": url.host,
    ]);

    Protocols.HTTP.Query q =
        Protocols.HTTP.do_method("GET", url, UNDEFINED, request_headers);

    if (q.status != 200 || !strlen(q.data() || ""))
    {
        return sprintf("There was an error running random.org request. (%d)",
                q.status);
    }

    return String.trim_all_whites(q.data());
}

private string get_pikerandom(int start, int end)
{
    random_seed(time());
    return sprintf("%d", random(end-start)+start);;
}

void create()
{
    ::create();

    re = Regexp.PCRE.Widestring("^!(random|pikerandom)( |)([0-9]+|[0-9]+ [0-9]+|)$");
}

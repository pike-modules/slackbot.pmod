constant name = "handler";
constant priority = 0;

protected multiset(string) commands = (< >);

mapping get_action(mapping(string:mixed) data);

bool applies(mapping(string:mixed) data)
{
    return commands[data?->text];
}


protected mixed query(string what)
{
    Config.Config.Section s = BOT->get_section(name);
    return s[?what];
}

protected void modvar(
    string key, string description, Config.Variable type, void|mixed def)
{
    BOT->defvar(name, key, description, type, def);
}

void create()
{
    define_variables();
}

void init()
{
}

void define_variables()
{
    modvar("debug", "Debug level", Config.Int, 0);
}

mapping(string:array(string)) get_commands()
{
    return ([ name : sort(indices(commands)) ]);
}
